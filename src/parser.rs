
use grid::*;

use std::io::*;
use std::fs::File;
use std::path::Path;
use std::str::Lines;

pub struct ParserInfo {
    pub field_size: u32,
    pub num_tasks: u32,
    pub fields: Vec<Vec<Option<u32>>>,
    pub number_literals: u32,
}

impl ParserInfo{
    pub fn new(size: u32, fields: Vec<Vec<Option<u32>>>, num_tasks: u32, number_literals: u32) -> Self{
        ParserInfo{
            field_size: size,
            fields,
            num_tasks,
            number_literals,
        }
    }

    pub fn print_field(&self){

        for line in self.fields.iter(){
            print!("| ");
            for ch in line.iter(){
                match ch{
                    Some(num) => print!("{} ", num),
                    None => print!("_ "),
                }
            }
            print!("|\n");
        }
    }

    pub fn to_grid(self) -> Grid{
        let mut cells = Vec::new();
        for x in self.fields.iter(){
            for y in x.iter(){
                if let Some(number) = y{
                    cells.push(Cell::Value(*number as usize -1 /* we start counting at zero */));
                }else{
                    cells.push(Cell::Variable);
                }
            }
        }

        let field_size = (self.fields.len() as f32).sqrt() as usize;
        //println!("Using field size of: {}", field_size);

        Grid::new( cells, field_size )
    }
}

///Parses soduku file to a sodoku input
pub fn parser_file(path: &Path) -> ParserInfo{

    let file = File::open(path).expect("Failed to find file!");
    let mut reader = BufReader::new(file);
    let mut buffer = String::new();
    reader.read_to_string(&mut buffer);

    //Our buffer is:
    //println!("Buffer is:\n{}", buffer);

    let vec: Vec<Vec<Option<u32>>> = Vec::new();
    let mut parse_info = ParserInfo::new(0, vec, 0, 0);

    //Check for the puzzle size
    for line in buffer.lines(){

        //Check if we can find the size
        if parse_info.field_size == 0{
            parse_size(line, &mut parse_info);
            //Update the length of the longest possible literal
            parse_info.number_literals = num_u32_characters(parse_info.field_size);
            //println!("Number of empty literals is {} for {} size", num_u32_characters(parse_info.field_size), parse_info.field_size);
        }

        //Check if be can find the number of tasks
        if parse_info.num_tasks == 0{
            parse_number_tasks(line, &mut parse_info);
        }

        //don't use line (currently)
        if line.starts_with("+---"){
            //println!("EmptyLine");
            continue
        }
        //Parse whole line into vec
        if line.starts_with("| "){
            //println!("Work line");
            parse_info_line(line, &mut parse_info);
        }
    }

    //parse_info.print_field();
    parse_info
}

fn parse_info_line(line: &str, info: &mut ParserInfo){

    let mut empty_literal = String::new();
    for _ in 0..info.number_literals{
        empty_literal.push('_');
    }
    //println!("DEBUG: empty literal: {}", empty_literal);
    //Final line
    let mut line_vec: Vec<Option<u32>> = Vec::new();

    let mut string_buf = String::new();

    for ch in line.chars(){
        if ch != '|' && ch != ' '{
            //Push the current char into the buffer
            string_buf.push(ch);
            
        }else{
            //Recording some char ended here, lets tests what we got
            if string_buf == empty_literal{
                line_vec.push(None)
            }else{
                //Check if we have an empty string (happens on the start of a line)
                if string_buf == String::new(){
                    continue;
                }
                //We have a full number
                //println!("Parsing: {}", string_buf);
                let num: u32 = string_buf.parse().expect("Could not parse number!");
                line_vec.push(Some(num));
            }

            //We can delete the buffer since we need it for the next literal
            string_buf = String::new();
        }
    }
    //Finally add number to our buffer
    info.fields.push(line_vec);
}

fn parse_size(line: &str, info: &mut ParserInfo){
    if line.starts_with("puzzle size: "){
        let mut check_buff = String::new();
        let mut size_buf = String::new();
        for ch in line.chars(){
            if check_buff == "puzzle size: ".to_string(){
                if ch == 'x'{
                    break;
                }
                size_buf.push(ch);
            }else{
                check_buff.push(ch);
            }
        }

        let size: u32 = size_buf.parse().expect("failed to find size ");
        info.field_size = size;
        //println!("Size: {}", size);
    }
}

fn parse_number_tasks(line: &str, info: &mut ParserInfo){
    //Check number of tasks
    if line.starts_with("number of tasks: "){
        let mut check_buf = String::new();
        let mut number_buf = String::new();
        for ch in line.chars(){
            if check_buf == "number of tasks: ".to_string(){
                number_buf.push(ch);
            }else{
                check_buf.push(ch)
            }
        }
        let num_tasks: u32 = number_buf.parse().expect("Could not parse number into task size");
        info.num_tasks = num_tasks;
        //println!("Number of tasks is: {}", number_buf);
    }
}

///Returns the number of characters this u32 has
pub fn num_u32_characters(num: u32) -> u32{
    let mut start = 10;
    let mut count = 1;
    while num > start-1{
        start *= 10;
        count += 1;
    }

    count
}
